vmgmt
===

Just another VM management solution, meant for easy integration with
Ansible, and tangentially with systemd.

*Vmgmt* can create and manage Debian-based persistent virtual machines
using QEMU and (optionally) LVM as the block storage backend. These
persistent VMs will boot their own MBR and have their own separate
lifecycle. Vmgmt only provides enough provisioning infrastructure to
automatically install Debian on newly created virtual machines.

The tool's model of virtual machines is organized into *interfaces*,
each containing multiple *virtual machines* (each VM can only belong
to one interface). This allows separation of VMs into different
network categories. The actual routing/firewall configuration of the
resulting bridge interface is then left to the administrator and
outside the scope of vmgmt.

Vmgmt's configuration follows the long-standing UNIX tradition of
"simple files in the filesystem" (see *Configuration* below).

## Why?

There are many, many tools available in the "VM management" space. So
why this one? There are a few issues with the alternatives that didn't
neatly fit our requirements...

* *libvirt* is not very friendly to configuration management (except
  perhaps when used via dedicated modules in Ansible or something like
  that), besides it does not handle VM lifecycle at all, so there's no
  easy way to get from "this is the VM metadata" to "the VM is ready
  to run";
* *systemd-machines* also does not do provisioning;
* *[kvm-manager](https://0xacab.org/dkg/kvm-manager)* is pretty similar
  and lightweight but it expects you to install VMs manually;
* *Ganeti* meets most of our requirements but it's kind of overkill
  for single-server scenarios, with all its DRBD and seamless VM
  migration features -- we've found that we are not that interested in
  absolutely preserving our VMs at all costs: instead, we're happier
  to rebuild them from backups when necessary (on the *pet* to
  *cattle* spectrum, we see VMs as cattle pens);
* *Terraform* might be closer but it is a very complex and
  sophisticated product, so once again there is no particular reason
  to face its learning curve for what is ultimately a very simple
  scenario.

## Configuration

The tool expects its configuration in the form of a filesystem
hierarchy of *interfaces* and *virtual machines* below
/etc/vmgmt/system.

Configuration files consist of shell *attribute*=*value* statements to
set various attributes of the associated object.

Every interface has both a configuration file in
/etc/vmgmt/system/*interface*.conf, and a subdirectory
/etc/vmgmt/system/*interface* which in turn contains VM
configurations.

### Interfaces

Interface attributes:

* *ip* / *ip6* - IP addresses (IPv4 / IPv6) of the host on this
  interface.
* *netmask* / *netmask6* - Netmasks for the above. The IPv4 netmask
  should have the full IP-like form (e.g. 255.255.255.0), while the
  IPv6 netmask is expressed as the number of bits. Defaults are
  respectively 255.255.255.0 and 64.
* *additional_dns_names* - A list of additional unqualified DNS names
  for the gateway IP address that should be made available to VMs.

### Virtual machines

VM configuration files can specify the following attributes:

* *ip* / *ip6* - IP addresses (IPv4 / IPv6) for static assignment.
* *ram* - memory allocation (default *512m*)
* *cpu* - CPU allocation (default *1*)
* *volumes* - A list of space-separated volume specifications, which
  can be either:
  * a filename, which is interpreted as a QCOW2-formatted disk image,
    or
  * a LV specification of the form *VG*:*name*:*size*.

### SSH access

Vmgmt will automatically provision newly created VMs with an
authorized_keys file for the root account if you add them to
/etc/vmgmt/authorized_keys.

## Deployment

To install the vmgmt tool, use the Debian package. It takes care of
the necessary systemd integration and everything.

From a practical perspective, all it takes once you have a VM
configuration in place is to run the

```shell
update-vmgmt
```

command in order to create the new systemd units and set up the
necessary network interfaces.

### Provisioning config

It is possible to customize, to a certain extent, the VM provisioning
process, using environment variables placed in */etc/default/vmgmt*:

* `DEBIAN_DIST` - codename of the Debian distribution to install
  (default: *bullseye*)
* `PROVISION_PACKAGES` - packages to install by default on new VMs
  (default: *openssh-server wget tftp gnupg python3 python3-apt*)
* `APT_PROXY` - use a HTTP proxy for APT
* `CRYPTED_ROOT_PASSWORD` - by default newly installed VMs have their
  root password disabled, but if necessary it can be set to this
  (crypted) value

## Operations

After creating a new VM, and running *update-vmgmt*, it should
automatically start (TODO). But if it doesn't, run

```shell
systemctl start vmgmt@NAME
```

where NAME is the name of the new VM.

The first time a VM runs, it will netboot the Debian installer and
setup the virtual machine. Once this process is complete (a few
minutes, depending on network connection) the virtual machine will
reboot and it will be possible to access it using the configured SSH
root credentials.

It is possible to connect to the serial console of a virtual machine
for debugging purposes with the command:

```shell
vmgmt-console NAME
```

### IPv6-only networks

Vmgmt can run IPv6-only networks, but due to an [issue with the Debian
installer and stateful
DHCPv6](https://git.autistici.org/ai3/tools/vmgmt/-/issues/1), you
need to run an APT proxy, which will then be reachable by the
debian-installer using its link-local IPv6 address.

An example configuration that works, assuming you have *apt-cacher-ng*
installed and listening on port 3142:

##### /etc/vmgmt/system/net1.conf

```
ip6=2a01:4f8:140:ffe2::1
additional_dns_names=apt-cache
```

##### /etc/vmgmt/system/net1/vm1.conf

```
ip6=2a01:4f8:140:ffe2::19
volumes=vg0:root:10G
ram=1G
```

##### /etc/default/vmgmt

```
APT_PROXY=http://apt-cache:3142
```
