#!/bin/sh

usage() {
    echo "Usage: $0 up|down <interface>" >&2
    exit 2
}

if [ $# -ne 2 ]; then
    usage
fi

cmd="$1"
iface="$2"

pidfile=/run/vmgmt-dnsmasq-${iface}.pid

is_process_live() {
    if [ -e "$pidfile" ]; then
        pid=$(cat "$pidfile")
        if kill -0 $pid 2>/dev/null ; then
            return 0
        fi
    fi
    return 1
}

case "$1" in
    up)
        if is_process_live; then
            echo "dnsmasq for $iface is already running" >&2
            exit 0
        fi
        exec dnsmasq \
             --log-async \
             --user=vmgmt \
             --pid-file=${pidfile} \
             --conf-file=/var/lib/vmgmt/dnsmasq/${iface}/config
        ;;

    down)
        pid=$(cat $pidfile 2>/dev/null)
        if [ -z "$pid" ]; then
            echo "No pidfile found" >&2
            exit 0
        fi
        kill -TERM $pid
        ;;

    restart)
        # Sending a SIGHUP to dnsmasq does not actually reload the
        # configuration file, only secondary files.
        "$0" down $iface
        exec "$0" up $iface
        ;;

    reload)
        # reload is restart, but only if the daemon is already running.
        if ! is_process_live; then
            exit 0
        fi
        exec "$0" restart $iface
        ;;

    *)
        usage
        ;;
esac

exit 0
