# Shell library for vmgmt.

ETCDIR=${DESTDIR:-}/etc/vmgmt
ETCTREE=${ETCDIR}/system
LIBDIR=${DESTDIR:-}/usr/lib/vmgmt
RUNDIR=${DESTDIR:-}/var/lib/vmgmt

die() {
    echo "FATAL ERROR: $*" >&2
    exit 1
}

# Return the list of configured interfaces.
list_interfaces() {
    for name in $(find "$ETCTREE" -mindepth 1 -maxdepth 1 -type f -name '*.conf' -printf '%f\n'); do
        name="${name%.conf}"
        if echo "x$name" | grep -q '[^-a-zA-Z0-9]'; then
            continue
        fi
        echo "$name"
    done
}

# Returns the list of configured VMs for a specific interface (if an
# argument is provided) or for all interfaces.
list_vms() {
    local root="$ETCTREE" mindepth=2
    if [ $# -gt 0 ]; then
        root="$ETCTREE/$1"
        mindepth=1
    fi
    for name in $(find "$root" -mindepth $mindepth -type f -name '*.conf' -printf '%f\n'); do
        name="${name%.conf}"
        if echo "x$name" | grep -q '[^-a-zA-Z0-9]'; then
            continue
        fi
        echo "$name"
    done
}

# Fetch an interface configuration attribute.
get_interface_param() {
    local interface="$1" param="$2" default="$3"
    (. "${ETCTREE}/${interface}.conf"; eval "echo \"\${$param:-$default}\"")
}

interface_has_ipv4() {
    if [ -n "$(get_interface_param $1 ip)" ]; then
        return 0
    fi
    return 1
}

interface_has_ipv6() {
    if [ -n "$(get_interface_param $1 ip6)" ]; then
        return 0
    fi
    return 1
}

# Fetch a VM configuration attribute.
get_vm_param() {
    local interface="$1" vm="$2" param="$3" default="$4"
    (. "${ETCTREE}/${interface}/${vm}.conf"; eval "echo \"\${$param:-$default}\"")
}

# Get the interface from just the VM name.
get_vm_interface() {
    local vm="$1"
    local vmconf_rel="$(find "$ETCTREE" -mindepth 2 -maxdepth 2 -name "${vm}.conf" -type f -printf '%P\n')"
    if [ -z "$vmconf_rel" ]; then
        echo "Can't find VM configuration" >&2
        exit 1
    fi
    local interface="${vmconf_rel%/*}"
    if [ -z "$interface" ]; then
        echo "Can't detect interface name from VM configuration path!" >&2
        exit 1
    fi
    echo $interface
}

# Derive a MAC address from a VM name.
macaddr_from_name() {
    # reuse the QEmu OUI space.
    echo -n "52:54:00"
    echo -n "$1" | md5sum - | cut -c-6 | sed -e 's/\(..\)/:\1/g'
}

# Returns true if the argument is a volume spec (pool:name:size),
# false otherwise (i.e. raw  file or device).
is_volume_spec() {
    case "$1" in
        *:*:*)
            return 0
            ;;
    esac
    return 1
}

# Parse a volume spec (pool).
get_volume_pool() {
    echo "$1" | cut -d: -f1
}

# Parse a volume spec (name).
get_volume_name() {
    echo "$1" | cut -d: -f2
}

# Parse a volume spec (size).
get_volume_size() {
    echo "$1" | cut -d: -f3
}
