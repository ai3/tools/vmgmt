#!/bin/sh

. /usr/lib/vmgmt/vmgmt-lib.sh

export LC_ALL=C QEMU_AUDIO_DRV=none

usage() {
    echo "Usage: $0 up|down <vm>" >&2
    exit 2
}

### MAIN

if [ $# -ne 2 ]; then
    usage
fi

cmd=$1
vm=$2

# Find the VM and detect the interface name from the configuration file path.
interface=$(get_vm_interface "$vm")
vm_config_path="${ETCTREE}/${interface}/${vm}.conf"
monitor_sock="${RUNDIR}/monitor/${vm}"

monitor_cmd() {
    echo "$*" | socat - unix-connect:$monitor_sock
}

is_running() {
    monitor_cmd info status 2>/dev/null | grep running
    return $?
}

case "$1" in

    start)
        umask 077
        mkdir -p ${RUNDIR}/secrets/${vm} ${RUNDIR}/monitor /var/log/vmgmt

        . "$vm_config_path"

        vm_disk_opts=
        index=0
        for volume in $volumes; do
            bootflag=
            if [ $index -eq 0 ]; then
                bootflag=",bootindex=0"
            fi

            # Parse the pool:name volume spec.
            dev=
            format=
            if is_volume_spec $volume; then
                format=raw
                volume_name=$(get_volume_name $volume)
                volume_pool=$(get_volume_pool $volume)
                if [ -z "$volume_name" -o -z "$volume_pool" ]; then
                    echo "Invalid volume specification '${volume}'" >&2
                    exit 2
                fi
                dev="/dev/${volume_pool}/${vm}-${volume_name}-disk"
            else
                dev=$volume
                format=qcow2
            fi

            vm_disk_opts="$vm_disk_opts -object iothread,id=iothread${index}
                -drive file=${dev},format=${format},if=none,aio=threads,cache=none,id=drive-virtio-disk${index}
                -device virtio-blk-pci,drive=drive-virtio-disk${index},id=virtio-disk${index},scsi=off,iothread=iothread${index}${bootflag}"
            index=$(expr $index + 1)
        done

        mac=$(macaddr_from_name $vm)
        vm_network_opts="-netdev bridge,id=hostnet0,br=${interface}
                -device virtio-net-pci,netdev=hostnet0,id=net0,mac=${mac},bootindex=1"

        secrets_file=${RUNDIR}/secrets/${vm}/master-key.aes
        if [ ! -e "$secrets_file" ]; then
            head -c 32 /dev/urandom > "$secrets_file"
        fi

        echo "Starting VM ${vm}..." >&2
        exec /usr/bin/qemu-system-x86_64 \
             -name guest=${vm},debug-threads=on \
             -enable-kvm \
             -no-reboot \
             -runas vmgmt \
             -object secret,id=masterKey0,format=raw,file=${RUNDIR}/secrets/${vm}/master-key.aes \
             -machine q35,accel=kvm,usb=off,dump-guest-core=off,graphics=off \
             -cpu host \
             -smp ${cpu:-1} \
             -m ${ram:-512m} \
             -overcommit mem-lock=off \
             -display none \
             -no-user-config \
             -nodefaults \
             -rtc base=utc \
             -boot menu=off,strict=on \
             -object rng-random,id=objrng0,filename=/dev/urandom \
             -device virtio-rng-pci,rng=objrng0,id=rng0 \
             -monitor unix:${monitor_sock},server,nowait \
             -chardev pty,id=charserial0,logfile=/var/log/vmgmt/${vm}.log,logappend=on \
             -device isa-serial,chardev=charserial0,id=serial0 \
             -sandbox on,obsolete=deny,resourcecontrol=deny \
             -msg timestamp=on \
             $vm_disk_opts \
             $vm_network_opts
        ;;

    stop)
        # The stop action has to wait until the VM has
        # terminated, or systemd will SIGTERM it right away.
        echo "Shutting down VM $vm..." >&2
        monitor_cmd system_powerdown >/dev/null 2>&1

        count=0
        time_limit_seconds=${STOP_TIMEOUT_SECONDS:-60}
        while [ $count -lt $time_limit_seconds ]; do
            if ! is_running; then
                echo "VM stopped." >&2
                exit 0
            fi
            count=$(expr $count + 1)
            sleep 1
        done
        echo "VM did not shut down, bailing out" >&2
        exit 1
        ;;

    *)
        usage
        ;;

esac
