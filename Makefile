
LIBDIR := $(DESTDIR)/usr/lib/vmgmt
BINDIR := $(DESTDIR)/usr/bin
SBINDIR := $(DESTDIR)/usr/sbin

LIB_SCRIPTS = \
	src/dnsmasq.sh \
	src/qemu.sh
LIB_LIBS = \
	src/vmgmt-lib.sh

BIN_SCRIPTS = \
	src/vmgmt-console
SBIN_SCRIPTS = \
	src/update-vmgmt

all:

install:
	install -d $(LIBDIR)
	(for s in $(LIB_SCRIPTS); do \
	  install -m 755 $$s $(LIBDIR)/$$(basename $$s); done)
	(for s in $(LIB_LIBS); do \
	  install -m 644 $$s $(LIBDIR)/$$(basename $$s); done)
	install -d $(BINDIR)
	(for s in $(BIN_SCRIPTS); do \
	  install -m 755 $$s $(BINDIR)/$$(basename $$s); done)
	install -d $(SBINDIR)
	(for s in $(SBIN_SCRIPTS); do \
	  install -m 755 $$s $(SBINDIR)/$$(basename $$s); done)
